package twitter.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter.entity.User;
import twitter.forrm.LoginForm;
import twitter.forrm.SignForm;
import twitter.mapper.UserMapper;

@Service
public class LoginService {
	@Autowired
	private UserMapper userMapper;
	public void signUser(LoginForm form) {
		userMapper.loginMapper(logintUser(form));
	}
	public User loginUser(LoginForm form) {
		User entity = new User();
		BeanUtils.copyProperties(form, entity);
		return entity;
	}
}
