package twitter.service;

import twitter.entity.User;
import twitter.forrm.SignForm;
import twitter.mapper.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	private UserMapper userMapper;
	public void signUser(SignForm form) {
		userMapper.userMapper(registUser(form));
	}
	public User registUser(SignForm form) {
		User entity = new User();
		BeanUtils.copyProperties(form, entity);
		return entity;
	}	
}
