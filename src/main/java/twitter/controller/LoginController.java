package twitter.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import twitter.forrm.LoginForm;
import twitter.service.LoginService;

@Controller
public class LoginController {	
	@Autowired
	private LoginService loginService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String logIn(Model model) {
		LoginForm form = new LoginForm();
		model.addAttribute("title","ろぐいん");
		model.addAttribute("signForm", form);
		return "login";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String valid(@Valid @ModelAttribute LoginForm form, BindingResult result, Model model) {
    if (result.hasErrors()) {
        model.addAttribute("title", "エラー");
        model.addAttribute("message", "以下のエラーを解消してください");
    } else {
    	loginService.loginUser(form);
    	model.addAttribute("message", "ok");
    }
	return "login";
	}
}