package twitter.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import twitter.forrm.SignForm;
import twitter.service.UserService;

@Controller
public class SignupController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUp(Model model) {
		SignForm form = new SignForm();
		model.addAttribute("title","とうろく");
		model.addAttribute("signForm", form);
		model.addAttribute("message", "登録してね(*'▽')☆");
		return "signup";
	}
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String valid(@Valid @ModelAttribute SignForm form, BindingResult result, Model model) {
	    if (result.hasErrors()) {
	        model.addAttribute("title", "エラー");
	        model.addAttribute("message", "以下のエラーを解消してね(´;ω;｀)");
	    } else {
	    userService.registUser(form);
		model.addAttribute("message", "登録しました");
	    }
		return "signup";
	}
}

