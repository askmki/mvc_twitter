<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
  <body>
	<h1>
		<c:out value="${title}"></c:out>
	</h1>
	<p>
		<c:out value="${message}"></c:out>
	</p>
	<form:form modelAttribute="signForm">
	<div><form:errors path="*"/></div>
		<table>
			<tbody>
				<tr>
					<td><form:label path="account">アカウント名：</form:label></td>
					<td><form:input path="account" size="20" /></td>
				</tr>
				<tr>
					<td><form:label path="password">パスワード：</form:label></td>
					<td><form:input path="password" cols="20" /></td>
				</tr>
			</tbody>
		</table>
		<input type="submit" value="★ログイン★" />
		<br />
		<a href="./">戻る</a>
	</form:form>
  </body>
</html>